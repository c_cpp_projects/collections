cmake_minimum_required(VERSION 3.16)

project(
  Collections
  VERSION 0.0.1
  DESCRIPTION "Various collections in C."
  HOMEPAGE_URL "https://gitlab.com/c_cpp_projects/collections"
  LANGUAGES C
)

set(CMAKE_MESSAGE_LOG_LEVEL VERBOSE)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_C_EXTENSIONS OFF)

set(HOME_BIN_DIR ${CMAKE_HOME_DIRECTORY}/bin)
set(HOME_INC_DIR ${CMAKE_HOME_DIRECTORY}/include)
set(HOME_LIB_DIR ${CMAKE_HOME_DIRECTORY}/lib)
set(HOME_SRC_DIR ${CMAKE_HOME_DIRECTORY}/src)


###### DO NOT CHANGE! ######

set(
  LIB_COLLECTIONS ${PROJECT_NAME}
  CACHE INTERNAL
  "The global target of the '${PROJECT_NAME}' project."
)



###### Configurable variables ######

function(add_cmake_config OPTION DESCRIPTION DEFAULT IS_DEFINITION)
  # Add OPTION as a cmake cache option.
  set(
    LIB_${OPTION} ${DEFAULT}
    CACHE BOOL
    ${DESCRIPTION}
  )

  message(VERBOSE "Added CMakeCache BOOL option \"LIB_${OPTION}\" (value: ${LIB_${OPTION}}).")

  # Check if OPTION is a compile time definition.
  if(IS_DEFINITION)
    # If OPTION is set to ON, then define it as 1(=TRUE), otherwise as 0(=FALSE).
    if(${LIB_${OPTION}})
      add_compile_definitions(
        ${OPTION}=1
      )
    else()
      add_compile_definitions(
        ${OPTION}=0
      )
    endif()
  endif()

endfunction()

add_cmake_config(
  COLLECTIONS_TEST
  "Enable/Disable the tests for ${PROJECT_NAME}."
  ON
  FALSE
)

add_cmake_config(
  COLLECTIONS_TEST_LOG
  "Enable/Disable logging for test subdir of ${PROJECT_NAME}."
  ON
  FALSE
)

add_cmake_config(
  COLLECTIONS_ENABLE_THREAD_SAFETY
  "Enable/Disable thread safety support."
  ON
  TRUE
)

add_cmake_config(
  COLLECTIONS_TEST_THREAD_SAFETY
  "Enable/Disable support for thread safety test. If this is activated, \
  then all functions will be artificially slowed down. DO NOT \
  USE THIS IN PRODUCTION CODE."
  OFF
  TRUE
)

add_cmake_config(
  COLLECTIONS_ANSI_ENABLE
  "Enable/Disable the ansi macro support."
  ON
  TRUE
)



###### Add compile definitions ######

# Add project version as compile definitions.
add_compile_definitions(
  LIB_COLLECTIONS_VERSION="${PROJECT_VERSION}"
  LIB_COLLECTIONS_VERSION_CODE=${PROJECT_VERSION_MAJOR}${PROJECT_VERSION_MINOR}${PROJECT_VERSION_PATCH}
  LIB_COLLECTIONS_VERSION_MAJOR=${PROJECT_VERSION_MAJOR}
  LIB_COLLECTIONS_VERSION_MINOR=${PROJECT_VERSION_MINOR}
  LIB_COLLECTIONS_VERSION_PATCH=${PROJECT_VERSION_PATCH}
)

# Add build type
if("Release" STREQUAL ${CMAKE_BUILD_TYPE})
  add_compile_definitions(
    COLLECTIONS_IS_RELEASE_BUILD
  )
else()
  add_compile_definitions(
      COLLECTIONS_IS_DEBUG_BUILD
  )
endif()




###### Configure compiler ######

if(CMAKE_COMPILER_IS_GNUCC)
  # Add all warnings
  add_compile_options(
    -Wall -Wextra -Wconversion -Wpedantic -Wformat
    -Wformat-overflow -Wnonnull -Winit-self -Wmain
    -Wparentheses -Wreturn-type -Wswitch -Wshift-overflow
    -Wswitch-default -Wswitch-enum -Walloc-zero
    -Wduplicated-branches -Wduplicated-cond -Wfloat-equal
  )
endif()



###### Add subdirs ######

add_subdirectory(src)

if(${LIB_COLLECTIONS_TEST})
  enable_testing()
  add_subdirectory(test)
endif()