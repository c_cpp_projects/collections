#ifndef COLLECTIONS_INCLUDE_UTILS_ANSI_ESCAPE_H
#define COLLECTIONS_INCLUDE_UTILS_ANSI_ESCAPE_H

#include "macros.h"



/******************** ANSI Escape Codes ********************/
// See https://en.wikipedia.org/wiki/ANSI_escape_code#Colors



#ifndef COLLECTIONS_ANSI_ENABLE
#  define COLLECTIONS_ANSI_ENABLE 0
#endif  // COLLECTIONS_ANSI_ENABLE



#if (0 != COLLECTIONS_ANSI_ENABLE)

#  define COLLECTIONS_ANSI_ESC     "\x1B["
#  define COLLECTIONS_ANSI_DEFAULT COLLECTIONS_ANSI_ESC "0m"


#  define COLLECTIONS_ANSI_BOLD      "1;"
#  define COLLECTIONS_ANSI_FAINT     "2;"
#  define COLLECTIONS_ANSI_ITALIC    "3;"
#  define COLLECTIONS_ANSI_UNDERLINE "4;"


/**
 * @brief Set the font type.
 * 
 *        You can set the @c font by combining @b COLLECTIONS_ANSI_BOLD ,
 *        @b COLLECTIONS_ANSI_ITALIC and @b COLLECTIONS_ANSI_UNDERLINE .
 * 
 * @param font - Determines the font type.
 */
#  define COLLECTIONS_ANSI_FONT(font) COLLECTIONS_ANSI_ESC font "37m"


/**
 * @brief Set the foreground color with a specific font type.
 * 
 *        You can set the @c font by combining @b COLLECTIONS_ANSI_BOLD ,
 *        @b COLLECTIONS_ANSI_ITALIC and @b COLLECTIONS_ANSI_UNDERLINE .
 * 
 * @param color - uint8_t that determines the color.
 * @param font - Determines the font type.
 */
#  define COLLECTIONS_ANSI_FG_FONT(color, font) \
    COLLECTIONS_ANSI_ESC font "38;5;" COLLECTIONS_STRINGIFY(color) "m"

/**
 * @brief Set the foreground color.
 * 
 * @param color - uint8_t that determines the color.
 */
#  define COLLECTIONS_ANSI_FG(color) COLLECTIONS_ANSI_FG_FONT(color, "")


/**
 * @brief Set the background color with a specific font type.
 * 
 *        You can set the @c font by combining @b COLLECTIONS_ANSI_BOLD ,
 *        @b COLLECTIONS_ANSI_ITALIC and @b COLLECTIONS_ANSI_UNDERLINE .
 * 
 * @param color - uint8_t that determines the color.
 * @param font - Determines the font type.
 */
#  define COLLECTIONS_ANSI_BG_FONT(color, font) \
    COLLECTIONS_ANSI_ESC font "48;5;" COLLECTIONS_STRINGIFY(color) "m"


/**
 * @brief Set the background color.
 * 
 * @param color - uint8_t that determines the color.
 */
#  define COLLECTIONS_ANSI_BG(color) COLLECTIONS_ANSI_BG_FONT(color, "")

#else  // COLLECTIONS_ANSI_ENABLE

#  define COLLECTIONS_ANSI_ESC
#  define COLLECTIONS_ANSI_DEFAULT
#  define COLLECTIONS_ANSI_BOLD
#  define COLLECTIONS_ANSI_FAINT
#  define COLLECTIONS_ANSI_ITALIC
#  define COLLECTIONS_ANSI_UNDERLINE
#  define COLLECTIONS_ANSI_FONT(font)
#  define COLLECTIONS_ANSI_FG_FONT(color, font)
#  define COLLECTIONS_ANSI_FG(color)
#  define COLLECTIONS_ANSI_BG_FONT(color, font)
#  define COLLECTIONS_ANSI_BG(color)

#endif  // COLLECTIONS_ANSI_ENABLE



#endif  // COLLECTIONS_INCLUDE_UTILS_ANSI_ESCAPE_H