#ifndef COLLECTIONS_INCLUDE_UTILS_ASSERT_H
#define COLLECTIONS_INCLUDE_UTILS_ASSERT_H

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "ansi_escape.h"
#include "build_type.h"
#include "macros.h"



#define COLLECTIONS_CURRENT_POSITION __FILE__ ":" COLLECTIONS_TO_STRING(__LINE__)

/**
 * @brief Get the name of the function currently being defined.
 */
#ifndef COLLECTIONS_FUNCTION
#  ifdef COLLECTIONS_IS_RELEASE_BUILD
#    define COLLECTIONS_FUNCTION ((char*) NULL)
#  else
#    define COLLECTIONS_FUNCTION __ASSERT_FUNCTION
#  endif
#endif  // COLLECTIONS_FUNCTION

#ifndef COLLECTIONS_ASSERT_FUNC
#  define COLLECTIONS_ASSERT_FUNC(expr, file, line, function) \
    __assert_fail(#expr, file, line, function);
#endif  // COLLECTIONS_ASSERT_FUNC



/**
 * @brief Assert with a formatted message.
 */
#ifndef COLLECTIONS_FASSERT

#  ifdef COLLECTIONS_IS_DEBUG_BUILD

#    define COLLECTIONS_ASSERT_FAIL_MSG                    \
      COLLECTIONS_ANSI_FG_FONT(202, COLLECTIONS_ANSI_BOLD) \
      "[ASSERTION "                                        \
      "FAILED]" COLLECTIONS_ANSI_DEFAULT

#    define COLLECTIONS_ERRNO_MSG \
      COLLECTIONS_ANSI_FG(196)    \
      "errno:" COLLECTIONS_ANSI_DEFAULT

#    define COLLECTIONS_FAINT_MSG(msg) \
      COLLECTIONS_ANSI_FG_FONT(244, COLLECTIONS_ANSI_FAINT) msg COLLECTIONS_ANSI_DEFAULT

#    define COLLECTIONS_BOLD_MSG(msg) \
      COLLECTIONS_ANSI_FONT(COLLECTIONS_ANSI_BOLD) msg COLLECTIONS_ANSI_DEFAULT



#    define COLLECTIONS_FASSERT(expr, format, ...)                                     \
      if (!(expr)) {                                                                   \
        if ((0 == strcmp("COLLECTIONS_ASSERT%u", format)) ||                           \
            (0 == strcmp("", format))) {                                               \
          fprintf(stderr,                                                              \
                  COLLECTIONS_ASSERT_FAIL_MSG "\n" COLLECTIONS_ERRNO_MSG " %s"         \
                                              "\n" COLLECTIONS_BOLD_MSG("File: ")      \
                                                COLLECTIONS_FAINT_MSG("%s") "\n",      \
                  ((0 == errno) ? COLLECTIONS_FAINT_MSG("None") : strerror(errno)),    \
                  COLLECTIONS_CURRENT_POSITION);                                       \
        } else {                                                                       \
          fprintf(stderr,                                                              \
                  COLLECTIONS_ASSERT_FAIL_MSG                                          \
                  "\n" COLLECTIONS_ERRNO_MSG " %s"                                     \
                  "\n" COLLECTIONS_BOLD_MSG("File: ")                                  \
                    COLLECTIONS_FAINT_MSG("%s") "\n" COLLECTIONS_BOLD_MSG("Message: ") \
                      format "\n",                                                     \
                  ((0 == errno) ? COLLECTIONS_FAINT_MSG("None") : strerror(errno)),    \
                  COLLECTIONS_CURRENT_POSITION,                                        \
                  ##__VA_ARGS__);                                                      \
        }                                                                              \
                                                                                       \
        COLLECTIONS_ASSERT_FUNC(expr, __FILE__, __LINE__, COLLECTIONS_FUNCTION);       \
      }

#  else  // COLLECTIONS_IS_DEBUG_BUILD
#    define COLLECTIONS_FASSERT(expr, format, ...)
#  endif  // COLLECTIONS_IS_DEBUG_BUILD

#endif  // COLLECTIONS_FASSERT



/**
 * @brief Assert without a formatted message.
 */
#define COLLECTIONS_ASSERT(expr) COLLECTIONS_FASSERT(expr, "COLLECTIONS_ASSERT%u", 0)



#endif  // COLLECTIONS_INCLUDE_UTILS_ASSERT_H