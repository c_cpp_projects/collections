#ifndef COLLECTIONS_INCLUDE_UTILS_TYPES_H
#define COLLECTIONS_INCLUDE_UTILS_TYPES_H

#include <stddef.h>



/**
 * @brief Type alias for a pointer to the bytes of an element
 *        that is stored in a collection.
 *
 *        This is not a typedef so 'const void*' does the right thing.
 */
#define Collections_ElementPtr void*

typedef struct {
  void* (*calloc)(size_t n, size_t size);
  void (*free)(void*);
} Collections_Allocator;

typedef struct {
  int (*init)(void** pMutex);
  int (*lock)(void* pMutex);
  int (*unlock)(void* pMutex);
  int (*destroy)(void* pMutex);
} Collections_Mutex;



#endif  // COLLECTIONS_INCLUDE_UTILS_TYPES_H