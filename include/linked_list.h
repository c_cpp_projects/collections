#ifndef COLLECTIONS_INCLUDE_LINKED_LIST_H
#define COLLECTIONS_INCLUDE_LINKED_LIST_H

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

#include "utils/assert.h"
#include "utils/macros.h"
#include "utils/types.h"



#define LinkedList_ElementPtr Collections_ElementPtr

COLLECTIONS_DEFINE(LinkedList);

typedef int (*LinkedList_CompareCb)(const LinkedList_ElementPtr a,
                                    const LinkedList_ElementPtr b);

typedef void (*LinkedList_CopyCb)(LinkedList_ElementPtr restrict destination,
                                  const LinkedList_ElementPtr restrict source,
                                  size_t size);

/**
 * @brief Use this callback if you want to execute an additional,
 *        cleanup routine for @c pElement , before it gets removed.
 *
 *        @b NEVER call @c free on the @c void* provided to
 *        this callback.
 */
typedef void (*LinkedList_DestroyCb)(LinkedList_ElementPtr restrict pElement,
                                     size_t size);

typedef void (*LinkedList_ForEachCb)(void* pUserData, size_t index,
                                     LinkedList_ElementPtr element);

typedef bool (*LinkedList_TestContainsCb)(void* pUserData, const void* pKey,
                                          const LinkedList_ElementPtr pElement);

typedef bool (*LinkedList_TestWhereCb)(void* pUserData, size_t index,
                                       const LinkedList_ElementPtr element);

typedef Collections_Allocator LinkedList_Allocator;
#if (0 != COLLECTIONS_ENABLE_THREAD_SAFETY)
typedef Collections_Mutex LinkedList_Mutex;
#endif

#define LINKED_LIST_FOR_EACH(pointerType, pItem, list)                          \
  for (pointerType pItem = (pointerType) LinkedList_first(list); NULL != pItem; \
       pItem = (pointerType) LinkedList_next(list, pItem))

/**
 * @brief Add an element to @c list . Nothing happens if @c list is @b NULL .
 *
 * @param pRawElement A pointer to the raw element data you want to add. The
 *                    @c LinkedList_CopyCb of @c list is used to copy the data
 *                    to which @c pRawElement points to.
 */
void
LinkedList_add(LinkedList list, const void* pRawElement);

/**
 * @brief Copy all elements from @c source to @c destination . Nothing
 *        happens if @c destination or @c source is, or both are @b NULL .
 *
 *        @c source and @c destination have to hold elements of the
 *        same type, meaning their @b elementSize has to be the same.
 */
void
LinkedList_addAll(LinkedList destination, const LinkedList source);

/**
 * @brief Add all elements the from the array @c base to @c list .
 *
 *        The size of an element in the array has to be equal to the
 *        size of an element from @c list , otherwise you enter the
 *        domain of undefined behaivour.
 *
 *        Nothing happens if @c list or @c base is @b NULL .
 *
 *
 * @param list The @c LinkedList to which you want to append
 *             all elements from the @c base array.
 *
 * @param base Basepointer of the array.
 *
 * @param length The length of the @c base array.
 */
void
LinkedList_addArray(LinkedList list, const void* base, size_t length);

/**
 * @brief Search for @c pKey in @c list using the binary search algorithm.
 *
 *
 * @param compare Returns 0 if element matches the key.
 *
 *                Returns any positive number, but not 0, when key
 *                appears after middle element.
 *
 *                Returns any negative number, when key
 *                appears before middle element.
 *
 *
 * @returns A @c LinkedList_ElementPtr the the stored element which matches
 *          @c pKey, or @b NULL if there is none.
 *
 *          Also returns @b NULL if @c list or @c compare is @b NULL .
 */
LinkedList_ElementPtr
LinkedList_bsearch(const LinkedList list, const void* pKey,
                   const LinkedList_CompareCb compare);

/**
 * @brief Remove every element in @c list. Nothing happens if @c list is @b NULL .
 *
 *        @c LinkedList_clear is @b NOT the same as @c LinkedList_destroy, because
 *        @c list can still be used after it has been cleared, whereas
 *        after calling @c LinkedList_destroy on it, the LinkedList must
 *        not be used anymore to avoid undefined behaivour.
 *
 *        The @c LinkedList_DestroyCb of @c list , if it is @b NON-NULL,
 *        gets called before each element gets removed.
 */
void
LinkedList_clear(LinkedList list);

/**
 * @brief Check if any element in @c list is equal to the element pointed to
 *        by @c pKey . Nothing happens if @c list or @c contains , or both are
 *        @b NULL .
 *
 *
 * @param pUserData A pointer that is passed to @c contains that can be used
 *                  to retrieve the found element.
 *
 * @param start Check every element in @c list from index start to the end.
 *
 * @param contains A callback for comparing each element with @c pKey .
 *
 *                Returns @b true if the element at index satisfies
 *                the test condition, else @b false.
 *
 *
 * @return @b true if @c list contains @c pKey .
 *         @b false if @c list does not contain @c pKey .
 *         @b false if @c start >= @c LinkedList_size() .
 */
bool
LinkedList_contains(const LinkedList list, void* pUserData, const void* pKey,
                    size_t start, const LinkedList_TestContainsCb contains);

/**
 * @brief Returns a copy of @c source . Nothing happens if @c source is @b NULL .
 *
 *        The copied @c LinkedList has the same @c LinkedList_CopyCb and
 *        @c LinkedList_DestroyCb as @c source .
 *
 *
 * @param allocator The @c LinkedList_Allocator used for dynamic memory
 *                  operations of the new list.
 *
 * @param mutex The @c LinkedList_Mutex which should be used for the new list.
 */
LinkedList
LinkedList_clone(const LinkedList source,
                 LinkedList_Allocator allocator
                   COLLECTIONS_THREAD_SAFETY_PARAM(LinkedList_Mutex, mutexFunctions));

/**
 * @brief Allocate an empty new @c LinkedList.
 *
 *
 * @param elementSize The size of an element that is stored in this element.
 *                    Only matters if @c copy is @b NON-NULL .
 *
 * @param copy The @c LinkedList_CopyCb that is used for adding new elements
 *             to this @c LinkedList . This parameter must be @b NON-NULL .
 *
 * @param destroy The @c LinkedList_DestroyCb is used for clearing/destroying the
 *                elements in this @c LinkedList.
 *
 *                DO NOT CALL @c free on the @c LinkedList_ElementPtr provided
 *                by this callback.
 *
 * @param allocator The @c LinkedList_Allocator used for dynamic memory operations.
 *
 * @param mutex The @c LinkedList_Mutex which should be used if
 *              @b COLLECTIONS_ENABLE_THREAD_SAFETY is not 0.
 */
LinkedList
LinkedList_create(size_t elementSize, const LinkedList_CopyCb copy,
                  const LinkedList_DestroyCb destroy,
                  LinkedList_Allocator allocator
                    COLLECTIONS_THREAD_SAFETY_PARAM(LinkedList_Mutex, mutex));

/**
 * @brief Destroy @c list . After a call to this function @c list should not be
 *        used with any other functions anymore.
 */
void
LinkedList_destroy(LinkedList list);

/**
 * @brief Get the @c LinkedList_ElementPtr at @c index from @c list .
 *
 *
 * @param index The index at which your desired element is placed.
 *              If the condition 0 =< @c index < @c LinkedList_size() is not
 *              fulfilled, then @b NULL gets returned.
 */
LinkedList_ElementPtr
LinkedList_elementAt(const LinkedList list, size_t index);

/**
 * @brief Check if @c listA and @c listB are equal.
 *
 *
 * @param compare Callback used to evaluate if @c a and @c b are equal.
 *                Returns 0, if @c a and @c b are EQUAL.
 *                Returns any integer that is not 0, if @c a and @c b are NOT EQUAL.
 *
 * @param checkOrder @b true if you want to check the order of
 *                   the elements.
 *
 *
 * @return @b true if @c listA is equal to @c listB , else @b false.
 *         Also returns @b false , if @c compare is @b NULL .
 */
bool
LinkedList_equal(const LinkedList listA, const LinkedList listB,
                 const LinkedList_CompareCb compare, bool checkOrder);

/**
 * @brief Check if @c list and the @c base array are equal.
 *
 *        The size of an element in the array has to be equal to the
 *        size of an element from @c list , otherwise you enter the
 *        domain of undefined behaivour.
 *
 *
 * @param base Basepointer of the array.
 *
 * @param length The length of the @c base array.
 *
 * @param compare Function used to evaluate if @c a and @c b are equal.
 *                Returns 0, if @c a and @c b are EQUAL.
 *                Returns any integer that is not 0, if @c a and @c b are NOT EQUAL.
 *
 * @param checkOrder @b true if you want to check the order of
 *                   the elements.
 *
 *
 * @return @b true if @c list is equal to @c base , else @b false.
 *         Also returns @b false , if @c compare is @b NULL .
 */
bool
LinkedList_equalArray(const LinkedList list, const void* base, size_t length,
                      const LinkedList_CompareCb compare, bool checkOrder);

/**
 * @brief Returns a @c LinkedList_ElementPtr to the first element in @c list ,
 *        or @b NULL if @c list is empty. Nothing happens if @c list is @b NULL .
 */
LinkedList_ElementPtr
LinkedList_first(const LinkedList list);

/**
 * @brief Returns a @c LinkedList_ElementPtr to the first element in @c list ,
 *        that satisfies the provided test.
 *
 *
 * @param pUserData A pointer that is passed to the @c LinkedList_TestWhereCb.
 *
 * @param start Check every element in @c list from index starting from
 *              @c start to the end of @c list . If the condition
 *              @c start < @c LinkedList_size() is not met, then @b NULL
 *              gets returned.
 *
 * @param test The test condition the element has to satisfy. Returns
 *             @b true if the element satisfies test, else @b false.
 *
 *
 * @return The @c LinkedList_ElementPtr for the first element in @c list
 *         that satisfied @c test , or @b NULL if there is none. Also
 *         returns @b NULL if @c list or @c test , or both are @b NULL .
 */
LinkedList_ElementPtr
LinkedList_firstWhere(const LinkedList list, void* pUserData, size_t start,
                      const LinkedList_TestWhereCb test);

/**
 * @brief Iterate over every element in @c list . This is way more efficient
 *        than calling @c LinkedList_elementAt() for each index. This function
 *        is also more efficient than calling @c LinkedList_next() to get each,
 *        element but the speed difference is not as significant as with
 *        @c LinkedList_elementAt() . Nothing happens if @c list , or @c forEach
 *        or both are @b NULL .
 *
 *
 * @param userData A pointer that is passed to @c forEach .
 *
 * @param forEach The @c LinkedList_ForEachCb for itering through all
 *                elements in @c list .
 */
void
LinkedList_forEach(const LinkedList list, void* userData,
                   const LinkedList_ForEachCb forEach);

/**
 * @brief Insert an element into @c list .
 *
 *        This increases the size of @c list by one and shifts
 *        all elements at, or after @c index towards the end of
 *        the list.
 *
 *        The element is only inserted if the condition
 *        0 <= @c index < @c LinkedList_size() is met.
 *
 *
 * @param index The position where you want to insert the specified
 *              element.
 *
 * @param pRawElement A pointer to the raw element data you want to insert.
 *                    The @c LinkedList_CopyCb if responsible for copying the
 *                    data of your element into @c list .
 */
void
LinkedList_insert(LinkedList list, size_t index, const void* pRawElement);

/**
 * @return @b true if @c list IS empty or @b NULL , otherwise @b false.
 */
bool
LinkedList_isEmpty(const LinkedList list);

/**
 * @return @b true if @c list IS NOT empty, otherwise @b false. Also @b false if
 *         @c list is @b NULL .
 */
bool
LinkedList_isNotEmpty(const LinkedList list);

/**
 * @brief Returns a @c LinkedList_ElementPtr to the last element in @c list ,
 *        or @b NULL if @c list is empty. Nothing happens if @c list is @b NULL .
 */
LinkedList_ElementPtr
LinkedList_last(const LinkedList list);

/**
 * @brief Returns a @c LinkedList_ElementPtr to the last element in @c list ,
 *        that satisfies the provided test.
 *
 *
 * @param pUserData A pointer that is passed to @c test .
 *
 * @param start Check every element in @c list from index starting from
 *              @c start to the beginning of @c list . If the condition
 *              @c start < @c LinkedList_size() is not met, then @b NULL
 *              gets returned.
 *
 * @param test The test condition the element has to satisfy. Returns
 *             @b true if the element satisfies test, else @b false.
 *
 *
 * @return The @c LinkedList_ElementPtr for the last element in @c list
 *         that satisfied @c test , or @b NULL if there is none. Also
 *         returns @b NULL if @c list or @c test , or both are @b NULL .
 */
LinkedList_ElementPtr
LinkedList_lastWhere(const LinkedList list, void* pUserData, size_t start,
                     const LinkedList_TestWhereCb test);

/**
 * @brief Get the element after @c pCurrentElement .
 *
 *
 * @param pCurrentElement A @c LinkedList_ElementPtr to the current element.
 *
 *
 * @return A @c LinkedList_ElementPtr to the next element, or
 *         @b NULL if @c pCurrentElement is the last one. Also returns
 *         @b NULL if @c list or @c pCurrentElement or both are @b NULL .
 */
LinkedList_ElementPtr
LinkedList_next(const LinkedList list, const LinkedList_ElementPtr pCurrentElement);

/**
 * @brief Get the element before @c pCurrentElement .
 *
 *
 * @param pCurrentElement A @c LinkedList_ElementPtr to the current element.
 *
 *
 * @return A @c LinkedList_ElementPtr to the previous element, or
 *         @b NULL if @c pCurrentElement is the first one. Also returns
 *         @b NULL if @c list or @c pCurrentElement or both are @b NULL .
 */
LinkedList_ElementPtr
LinkedList_previous(const LinkedList list, const LinkedList_ElementPtr pCurrentElement);

/**
 * @brief Print the address of each node and their respective elements
 *        in @c list to @c file .
 *
 *
 * @param pFile The @c FILE* to which you want to print the output
 *              of this function. If @c pFile is @b NULL , then it
 *              defaults to @c stdout .
 */
void
LinkedList_print(const LinkedList list, FILE* pFile);

/**
 * @brief Put an element at @c index and get the old element.
 *
 *
 * @param index The index where you want to put @c pElement .
 *              If the condition 0 =< @c index < @c LinkedList_size() is not
 *              fulfilled, then the element is not put into @c list .
 *
 * @param pElement A pointer to the raw element data you want to put at @c index .
 *                 The @c LinkedList_CopyCb if responsible for copying the
 *                 data of your element into @c list .
 *
 * @param oldElementbuffer If @b NON-NULL , then copy the memory from the
 *                         old element at @c index into @c oldElementbuffer ,
 *                         otherwise the old element gets destroyed.
 *
 *                         If @c oldElementbuffer is smaller than the size of
 *                         an element in @c list , then you enter the domain of
 *                         undefined behaviour.
 *
 *
 * @return @b True if the element at @c index was successfully updated
 *         (and the old one therefore copied into @c oldElementbuffer ), or @b false
 *         if no element was removed. Also returns @b false if @c list is @b NULL .
 */
bool
LinkedList_put(LinkedList list, size_t index, const void* pRawElement,
               void* oldElementbuffer);

/**
 * @brief Remove the element at @c index from @c list .
 *
 *
 * @param index The index of the element you want to remove.
 *              If the condition 0 =< @c index < @c LinkedList_size() is not
 *              fulfilled, then no element is removed.
 *
 * @param removedElementbuffer If @b NON-NULL , then copy the memory from the
 *                             removed element at @c index into @c removedElementbuffer ,
 *                             otherwise the removed element gets destroyed.
 *
 *                             If @c removedElementbuffer is smaller than the size of
 *                             an element in @c list , then you enter the domain of
 *                             undefined behaviour.
 *
 *
 * @return @b True if the element at @c index was successfully removed
 *         (and therefore copied into @c removedElementbuffer ), or @b false
 *         if no element was removed. Also returns @b false if @c list if @b NULL .
 */
bool
LinkedList_removeAt(LinkedList list, size_t index, void* removedElementbuffer);

/**
 * @brief Remove the first element from @c list for which @c test
 *        evaluates to @b true .
 *
 *
 * @param pUserData A pointer that is passed to @c test .
 *
 * @param start Check every element in @c list from index starting from
 *              @c start to the end of @c list . If the condition
 *              @c start < @c LinkedList_size() is not met, then @b false
 *              gets returned.
 *
 * @param test The test condition the element has to satisfy for it to be removed.
 *             The @c LinkedList_TestWhereCb has to return @b true if the element
 *             satisfies test and should therefore be removed, return @b false if
 *             the element should not get removed.
 *
 * @param removedElementbuffer If @b NON-NULL , then copy the memory from the
 *                             removed element at @c index into @c removedElementbuffer ,
 *                             otherwise the removed element gets destroyed.
 *
 *                             If @c removedElementbuffer is smaller than the size of
 *                             an element in @c list , then you enter the domain of
 *                             undefined behaviour.
 *
 *
 * @return @b true if there was an element that satisfied @c test and was
 *         therfore removed from @c list (and possibly copied into
 *         @c removedElementbuffer ), or @b false if no element was removed.
 *         Also returns @b NULL if @c list or @c test , or both are @b NULL .
 */
bool
LinkedList_removeFirstWhere(LinkedList list, void* pUserData, size_t start,
                            const LinkedList_TestWhereCb test,
                            void* removedElementbuffer);

/**
 * @brief Remove the last element from @c list for which @c test
 *        evaluates to @b true .
 *
 *
 * @param pUserData A pointer that is passed to @c test .
 *
 * @param start Check every element in @c list from index starting from
 *              @c start to the beginning of @c list . If the condition
 *              @c start < @c LinkedList_size() is not met, then @b false
 *              gets returned.
 *
 * @param test The test condition the element has to satisfy for it to be removed.
 *             The @c LinkedList_TestWhereCb has to return @b true if the element
 *             satisfies test and should therefore be removed, return @b false if
 *             the element should not get removed.
 *
 * @param removedElementbuffer If @b NON-NULL , then copy the memory from the
 *                             removed element at @c index into @c removedElementbuffer ,
 *                             otherwise the removed element gets destroyed.
 *
 *                             If @c removedElementbuffer is smaller than the size of
 *                             an element in @c list , then you enter the domain of
 *                             undefined behaviour.
 *
 *
 * @return @b true if there was an element that satisfied @c test and was
 *         therfore removed from @c list (and possibly copied into
 *         @c removedElementbuffer ), or @b false if no element was removed.
 *         Also returns @b NULL if @c list or @c test , or both are @b NULL .
 */
bool
LinkedList_removeLastWhere(LinkedList list, void* pUserData, size_t start,
                           const LinkedList_TestWhereCb test, void* removedElementbuffer);

/**
 * @brief Remove all elements from @c list that satisfy @c test.
 *
 *        Nothing happens if @c list or @c test , or both are @b NULL .
 *
 *
 * @param pUserData A pointer that is passed to @c test .
 *
 * @param test The test condition the element has to satisfy for it to be removed.
 *             The @c LinkedList_TestWhereCb has to return @b true if the element
 *             satisfies test and should therefore be removed, return @b false if
 *             the element should not get removed.
 */
void
LinkedList_removeWhere(LinkedList list, void* pUserData,
                       const LinkedList_TestWhereCb test);

/**
 * @brief Reverse the order of @c list .
 *
 *        Nothing happens if @c list is @b NULL .
 */
void
LinkedList_reverse(LinkedList list);

/**
 * @brief Get the current size of @c list . The current size is stored in
 *        the @c list , therefore this function has an access time of O(1).
 *
 * @return The current size of @c list or @b 0 if @c list is @b NULL .
 */
size_t
LinkedList_size(const LinkedList list);

/**
 * @brief Sort @c list using the mergesort algorithm.
 *
 *        Nothing happens if @c list or @c compare , or both are @b NULL , or
 *        if @c LinkedList_size() <= 1.
 *
 *
 * @param compare Callback used to evaluate if @c a should be before or after @c b .
 *                Returns 0 if @c a should be after @c b .
 *                Returns NOT 0 if @c a should be before @c b .
 */
void
LinkedList_sort(LinkedList list, const LinkedList_CompareCb compare);

/**
 * @brief Copy all elements from @c list into @c buffer .
 *
 *        Nothing happens if @c list is @b NULL or empty.
 *
 *
 * @param list The @c LinkedList you want to convert to an array.
 *
 * @param buffer Pointer to a block of memory that is big enough to
 *               fit @c LinkedList_size() times the size of an element in
 *               @c list bytes.
 *
 *              If @c buffer is not big enough to fit all elements
 *              from @c list then you enter the domain of undefined
 *              behaviour.
 */
void
LinkedList_toArray(const LinkedList list, void* buffer);

#endif  // COLLECTIONS_INCLUDE_LINKED_LIST_H
