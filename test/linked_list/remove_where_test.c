#include "test_utils.h"

int
main() {
  LinkedList list =
    LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);

  TPerson persons1[] = {
    {-947, "Ermund"},
    {-544, "Martin"},
    {7444, "Hranov"},
  };
  TPerson persons2[] = {
    {423, "Peter"}, {-124, "Manfred"}, {441, "Norman"}, {-534, "Siegfried"}, {10, "Hias"},
    // {-10, "Hias"},
  };

  LinkedList_addArray(list, persons2, LENGTH_OF(persons2));

  printf("list:\n");
  LIST_PRINT(list, forEachPerson);

  int minId = 0;
  printf("Removing where id greater than %d\n", minId);
  LinkedList_removeWhere(list, &minId, removeWhereTPerson);
  printf("\n");

  printf("removed list:\n");
  LIST_PRINT(list, forEachPerson);

  printf("Adding persons1\n");
  LinkedList_add(list, &persons1[0]);
  LIST_PRINT(list, forEachPerson);

  TPerson* p1 = LinkedList_last(list);
  printTPerson(p1);

  LinkedList_destroy(list);

  return 0;
}