#include "test_utils.h"

#include <string.h>

int
main() {
  LinkedList list1 = LIST_CREATE(sizeof(TPerson), memcpy, destroyPerson);
  LinkedList list2 = LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);

  TPerson persons[] = {
    {423,  "Peter"     },
    {124,  "Manfred"   },
    {441,  "Norman"    },
    {534,  "Siegfried" },
    {714,  "Hans Peter"},
    {-947, "Ermund"    },
    {-544, "Martin"    },
    {416,  "Samuel"    },
    {7444, "Hranov"    },
    {10,   "Hias"      }
  };
  size_t i;
  for (i = 0; i < LENGTH_OF(persons) / 2; ++i) {
    LinkedList_add(list1, &persons[i]);
  }
  for (i = LENGTH_OF(persons) / 2; i < LENGTH_OF(persons); ++i) {
    LinkedList_add(list2, &persons[i]);
  }

  printf("list1:\n");
  LinkedList_forEach(list1, NULL, forEachPerson), printf("\n");

  printf("list2:\n");
  LinkedList_forEach(list2, NULL, forEachPerson), printf("\n");

  printf("AddAll from list2 to list1:\n");
  LinkedList_addAll(list1, list2), printf("\n");

  printf("list1:\n");
  LinkedList_forEach(list1, NULL, forEachPerson), printf("\n");

  printf("list2:\n");
  LinkedList_forEach(list2, NULL, forEachPerson), printf("\n");

  printf("AddAll list2, list1:\n");
  LinkedList_addAll(list2, list1), printf("\n");

  printf("list1:\n");
  LinkedList_forEach(list1, NULL, forEachPerson), printf("\n");

  printf("list2:\n");
  LinkedList_forEach(list2, NULL, forEachPerson), printf("\n");

  LinkedList_destroy(list1);
  LinkedList_destroy(list2);

  return 0;
}