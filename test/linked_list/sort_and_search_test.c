#include "test_utils.h"

int
main() {
  LinkedList list =
    LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);

  TPerson persons[] = {{423, "Peter"},
                       {124, "Manfred"},
                       {441, "Norman"},
                       {534, "Siegfried"},
                       {714, "Hans Peter"},
                       {-947, "Ermund"},
                       {-544, "Martin"},
                       {416, "Samuel"},
                       {7444, "Hranov"},
                       {10, "Hias"},
                       {487, "Siegbert"},
                       {-97, "Sragl"}};

  LinkedList_addArray(list, persons, LENGTH_OF(persons));

  printf("before sort:\n");
  LIST_PRINT(list, forEachPerson);

  LinkedList_sort(list, sortTPerson);

  printf("after sort:\n");
  LIST_PRINT(list, forEachPerson);

  TPerson key;
  key = (TPerson){10, "Peta"};
  // key.id = 99999;
  // strncpy(key.name, "test1", 30);

  TPerson* found = (TPerson*) LinkedList_bsearch(list, &key, searchTPerson);

  printf("key\n");
  printTPerson(&key), printf("\n");

  printf("found\n");
  printTPerson(found), printf("\n");

  LinkedList_destroy(list);

  return 0;
}