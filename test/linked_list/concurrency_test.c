#include "test_utils.h"


TPerson persons[] = {
  {-947, "Ermund"},
  {-544, "Martin"},
  {7444, "Hranov"},
  {423,  "Peter" },
 //   {-124, "Manfred"},
  //   {441,  "Norman" },
  //   {-534, "Siegfried"},
  //   {10,   "Hias"     },
  //   {7,    "Sragrl"   },
  //   {10,   "Norbert"  },
  //   {91,   "Hofer"    },
  //   {4441, "Lidl"     },
};
LinkedList gList = NULL;
size_t index;
thrd_t gThread;

int
threadFun(void* arg) {
  printf("Adding array at %lu\n", index);
  LinkedList_addArray(gList, persons, LENGTH_OF(persons));
  return 0;
}

void
createThread() {
  static bool created = false;
  if (created)
    return;

  if (thrd_success != thrd_create(&gThread, threadFun, NULL)) {
    printf("Failed to create thread!\n");
    abort();
  }

  created = true;
}

int
main() {
  gList = LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);

  for (index = 0; index < LENGTH_OF(persons); ++index) {
    printf("Adding %lu\n", index);
    LinkedList_add(gList, &persons[index]);
    createThread();
  }

  if (thrd_success != thrd_join(gThread, NULL)) {
    printf("Failed to join thread!\n");
    return 1;
  }

  LIST_PRINT(gList, forEachPerson);

  printf("Destroying...\n");
  LinkedList_destroy(gList);

  return 0;
}
