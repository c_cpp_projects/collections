#include "test_utils.h"

int
main() {
  LinkedList list = LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);

  TPerson persons[] = {{42, "Peter"},
                       {104, "Manfred"},
                       {141, "Norman"},
                       {340, "Siegfried"},
                       {414, "Hans Peter"},
                       {947, "Ermund"}};

  LinkedList_addArray(list, persons, LENGTH_OF(persons));
  LIST_PRINT(list, forEachPerson);



  // find should succeed
  TPerson key;
  key = persons[5];
  TPerson* found = (TPerson*) LinkedList_bsearch(list, &key, searchTPerson);

  printf("find succeed\n");
  printf("key\n");
  printTPerson(&key);
  printf("found\n");
  printTPerson(found);
  printf("\n");
  COLLECTIONS_ASSERT(NULL != found);



  // find should fail
  key = (TPerson){147802, "Nonaned"};
  found = (TPerson*) LinkedList_bsearch(list, &key, searchTPerson);

  printf("find fail\n");
  printf("key\n");
  printTPerson(&key);
  printf("found\n");
  printTPerson(found);
  printf("\n");
  COLLECTIONS_ASSERT(NULL == found);



  LinkedList_destroy(list);

  return 0;
}