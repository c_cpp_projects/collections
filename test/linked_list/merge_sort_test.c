#include "test_utils.h"

int
main() {
  LinkedList list =
    LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);
  LinkedList sortedList =
    LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);

  TPerson persons[] = {{423, "Peter"},
                       {124, "Manfred"},
                       {441, "Norman"},
                       {534, "Siegfried"},
                       {714, "Hans Pet"},
                       {-947, "Ermund"},
                       {-544, "Martin"},
                       {416, "Samuel"},
                       {7444, "Hranov"},
                       {10, "Hias"},
                       {487, "Siegbert"},
                       {-97, "Sragl"}};
  TPerson sortedPersons[LENGTH_OF(persons)];
  memcpy(sortedPersons, persons, sizeof(persons));
  qsort(sortedPersons, LENGTH_OF(sortedPersons), sizeof(TPerson), sortTPerson);

  LinkedList_addArray(list, persons, LENGTH_OF(persons));
  LinkedList_addArray(sortedList, sortedPersons, LENGTH_OF(sortedPersons));
  LinkedList_reverse(sortedList);

  printf("SORTED list:\n");
  LIST_PRINT(sortedList, forEachPerson);
  // LinkedList_print(sortedList);
  // printf("\n");

  printf("unsorted list BEFORE sort:\n");
  LIST_PRINT(list, forEachPerson);
  // LinkedList_print(sortedList);
  // printf("\n");

  LinkedList_sort(list, sortTPerson);

  printf("after sort:\n");
  LIST_PRINT(list, forEachPerson);
  // LinkedList_print(list);
  // printf("\n");

  bool equal = LinkedList_equal(sortedList, list, searchTPerson, true);
  printf("%sEqual\n\n", equal ? "" : "NOT ");

  LinkedList_destroy(list);
  LinkedList_destroy(sortedList);

  return equal ? 0 : 1;
}