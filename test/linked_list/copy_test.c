#include "test_utils.h"

int
main() {
  printf("\n");
  LinkedList list = LIST_CREATE(
    sizeof(const char*), copyCStringAddress, NULL);
  LinkedList copy = NULL;

  char* strings[] = {
    "first",
    "second",
    "third",
    "fourth",
  };

  printf("Print s1 - s4\n");
  printCString(strings[0]);
  printCString(strings[1]);
  printCString(strings[2]);
  printCString(strings[3]);
  printf("\n");



  LinkedList_add(list, &strings[0]);
  LinkedList_add(list, &strings[1]);
  LinkedList_add(list, &strings[2]);

  printf("Print list 1\n");
  LIST_PRINT(list, forEachCString);

  printf("Print copy 1\n");
  LIST_PRINT(copy, forEachCString);



  printf("Now copying...\n\n");
  copy = LIST_CLONE(list);

  printf("Print list 2\n");
  LIST_PRINT(list, forEachCString);

  printf("Print copy 2\n");
  LIST_PRINT(copy, forEachCString);



  printf("Now destroying list...\n\n");
  LinkedList_destroy(list);

  printf("Print copy 3\n");
  LIST_PRINT(copy, forEachCString);

  size_t i = 0;
  for (void* elem = LinkedList_first(copy); NULL != elem; elem = LinkedList_next(copy, elem)) {
    const char* s = *((char**) elem);
    printCString(s);
    COLLECTIONS_ASSERT(0 == strcmp(s, strings[i]));
    ++i;
  }

  i = LinkedList_size(copy) - 1;
  for (void* elem = LinkedList_last(copy); NULL != elem; elem = LinkedList_previous(copy, elem)) {
    const char* s = *((char**) elem);
    printCString(s);
    COLLECTIONS_ASSERT(0 == strcmp(s, strings[i]));
    --i;
  }

  printf("Now destroying copy...\n\n");
  LinkedList_destroy(copy);

  return 0;
}