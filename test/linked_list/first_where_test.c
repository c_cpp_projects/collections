#include "test_utils.h"

int
main() {
  LinkedList list = LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);

  const TPerson persons[] = {
    {-947, "Ermund"},
    {-544, "Martin"},
    {7444, "Hranov"},
    {441,  "Norman"},
    {10,   "Hias"  },
  };

  LinkedList_addArray(list, persons, LENGTH_OF(persons));

  printf("list:\n");
  LIST_PRINT(list, forEachPerson);

  int minId;
  TPerson* found;

  minId = 7000;
  found = (TPerson*) LinkedList_firstWhere(list, &minId, 0, wherePersonIdGreaterThan);
  printf("(should find) (start: %d) first where id greater than %d\n", 0, minId);
  printTPerson(found);
  printf("\n");
  COLLECTIONS_ASSERT(0 == compareTPerson(&persons[2], found));

  found = (TPerson*) LinkedList_firstWhere(list, &minId, 2, wherePersonIdGreaterThan);
  printf("(should find) (start: %d) first where id greater than %d\n", 2, minId);
  printTPerson(found);
  printf("\n");
  COLLECTIONS_ASSERT(0 == compareTPerson(&persons[2], found));

  found = (TPerson*) LinkedList_firstWhere(list, &minId, 3, wherePersonIdGreaterThan);
  printf("(should NOT find) (start: %d) first where id greater than %d\n", 3, minId);
  printTPerson(found);
  printf("\n");
  COLLECTIONS_ASSERT(NULL == found);

  found = (TPerson*) LinkedList_firstWhere(list, &minId, 10, wherePersonIdGreaterThan);
  printf("(should NOT find) (start: %d) first where id greater than %d\n", 10, minId);
  printTPerson(found);
  printf("\n");
  COLLECTIONS_ASSERT(NULL == found);

  minId = 100;
  found = (TPerson*) LinkedList_firstWhere(list, &minId, 3, wherePersonIdGreaterThan);
  printf("(should find) (start: %d) first where id greater than %d\n", 3, minId);
  printTPerson(found);
  printf("\n");
  COLLECTIONS_ASSERT(0 == compareTPerson(&persons[3], found));


  LinkedList_destroy(list);

  return 0;
}