#include "test_utils.h"

int
main() {
  LinkedList myList =
    LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);

  TPerson pers1 = {4, "Peter"};
  TPerson pers2 = {7, "Sragrl"};
  TPerson pers3 = {1, "Norbert"};
  TPerson newPerson = {3, "Manfre"};

  LinkedList_add(myList, &pers1);
  LinkedList_add(myList, &pers2);
  LinkedList_add(myList, &pers3);

  LinkedList_forEach(myList, NULL, forEachPerson);
  printf("\n");

  TPerson old;
  LinkedList_put(myList, 0, &newPerson, &old);
  printf("OLD: ");
  printTPerson(&old);
  printf("\n");

  COLLECTIONS_ASSERT(0 == memcmp(&old, &pers1, sizeof(TPerson)));

  LinkedList_forEach(myList, NULL, forEachPerson);

  LinkedList_destroy(myList);

  return 0;
}