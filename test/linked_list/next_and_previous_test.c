#include "test_utils.h"

int
main() {
  LinkedList list =
    LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);

  TPerson persons[] = {
    {4, "Peter"},
    {7, "Sragrl"},
    {10, "Norbert"},
    {541, "Manfred"},
    {91, "Hofer"},
    {4441, "Lidl"},
  };


  printf("Adding persons\n");
  LinkedList_addArray(list, persons, LENGTH_OF(persons));
  // for (size_t i = 0; i < LENGTH_OF(persons); ++i) {
  //   printTPerson(&persons[i]);
  //   LinkedList_add(list, &persons[i]);
  // }
  printf("\n");

  printf("LinkedList_next\n");
  for (TPerson* curr = LinkedList_first(list); NULL != curr; curr = LinkedList_next(list, curr)) {
    printTPerson(curr);
  }

  printf("\n");

  printf("LinkedList_previous\n");
  for (TPerson* curr = LinkedList_last(list); NULL != curr; curr = LinkedList_previous(list, curr)) {
    printTPerson(curr);
  }

  return 0;
}