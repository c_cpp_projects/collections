#include "test_utils.h"

int
main() {
  LinkedList list =
    LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);


  TPerson persons[] = {
    {-947, "Ermund"},
    {-544, "Martin"},
    {7444, "Hranov"},
    {423, "Peter"},
    {-124, "Manfred"},
    {441, "Norman"},
    {-534, "Siegfried"},
    {10, "Hias"},
  };
  TPerson insertMe = {4441, "Lidl"};
  size_t insertIdx = 2;

  LinkedList_addArray(list, persons, LENGTH_OF(persons));
  printf("BEFORE insertion:\n");
  LIST_PRINT(list, forEachPerson);

  printf("Inserting at %lu\n", insertIdx);
  forEachPerson(NULL, insertIdx, &insertMe);
  LinkedList_insert(list, insertIdx, &insertMe);
  printf("\n");

  printf("AFTER insertion:\n");
  LIST_PRINT(list, forEachPerson);

  COLLECTIONS_ASSERT(0 == memcmp(&insertMe, LinkedList_elementAt(list, insertIdx), sizeof(TPerson)));

  LinkedList_destroy(list);

  return 0;
}