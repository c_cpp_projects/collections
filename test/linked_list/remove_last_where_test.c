#include "test_utils.h"

int
main() {
  LinkedList list = LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);

  TPerson removeMe = {-1000, "Hias"};
  TPerson persons[] = {
    {-947, "Ermund"},
    {-544, "Martin"},
    {423,  "Peter" },
    {7444, "Hranov"},
    removeMe,
    {441,  "Norman"},
  };

  LinkedList_addArray(list, persons, LENGTH_OF(persons));

  printf("list:\n");
  LIST_PRINT(list, forEachPerson);

  int minId = 0;
  printf("Removing last where id smaller than %d\n", minId);
  TPerson removedPerson;
  bool isRemoved = LinkedList_removeLastWhere(
    list, &minId, LinkedList_size(list) - 1, wherePersonIdSmallerThan, &removedPerson);
  printf("\n");

  printf("Successfully removed: %s\n", isRemoved ? "true" : "false");
  COLLECTIONS_ASSERT(isRemoved);
  COLLECTIONS_ASSERT(0 == memcmp(&removedPerson, &removeMe, sizeof(TPerson)));

  printf("Removed:\n");
  printTPerson(&removedPerson);

  printf("removed list:\n");
  LIST_PRINT(list, forEachPerson);

  printf("last :\n");
  TPerson* p1 = LinkedList_last(list);
  COLLECTIONS_ASSERT(0 == memcmp(p1, &persons[LENGTH_OF(persons) - 1], sizeof(TPerson)));
  printTPerson(p1);

  LinkedList_destroy(list);

  return 0;
}