#include "test_utils.h"

int
main() {
  LinkedList list = LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);

  TPerson persons[] = {
    {-947, "Ermund"},
    {-544, "Martin"},
    {7444, "Hranov"},
    {423, "Peter"},
    {-124, "Manfred"},
    {441, "Norman"},
    {-534, "Siegfried"},
    {10, "Hias"},
  };

  LinkedList_addArray(list, persons, LENGTH_OF(persons));
  LIST_PRINT(list, forEachPerson);

  COLLECTIONS_ASSERT(
    LinkedList_equalArray(list, persons, LENGTH_OF(persons), compareTPerson, true));
  COLLECTIONS_ASSERT(
    LinkedList_equalArray(list, persons, LENGTH_OF(persons), compareTPerson, false));

  LinkedList_destroy(list);

  return 0;
}
