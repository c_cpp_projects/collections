#include "test_utils.h"

int
main() {
  LinkedList list =
    LIST_CREATE(sizeof(TPerson), copyPerson, destroyPerson);

  TPerson p1 = {423, "Peter"};
  TPerson p2 = {124, "Manfred"};
  TPerson p3 = {-544, "Martin"};

  LinkedList_add(list, &p1);
  LIST_PRINT(list, forEachPerson);

  LinkedList_removeAt(list, 0, false);
  LIST_PRINT(list, forEachPerson);

  LinkedList_add(list, &p2);
  LIST_PRINT(list, forEachPerson);

  TPerson* p = LinkedList_last(list);
  printTPerson(p);

  LinkedList_destroy(list);

  return 0;
}