include_directories(
  ${HOME_INC_DIR}
)

if(${LIB_COLLECTIONS_TEST_LOG})
  add_compile_definitions(
    LIB_COLLECTIONS_TEST_LOG=1    
  )
endif()

# Ignore certain warnings for tests
add_compile_options(
  -Wno-unused-parameter
  -Wno-unused-variable
)

# Use this function to add tests
#
# If you have the file "utils_test.c", then
# you can call 
# add_test_executable(utils_test)
function(add_test_executable BASEFILE_NAME)
  # Check if correct # of args was passed
  if(NOT ${ARGC} EQUAL 1)
    message(FATAL_ERROR "Call to 'add_test_executable' with ${ARGC} arguments instead of 1.")
  endif()

  # Get basename of current source dir
  string(FIND ${CMAKE_CURRENT_SOURCE_DIR} "/" lastIndexOfSeperator REVERSE)
  math(EXPR lastIndexOfSeperator "${lastIndexOfSeperator} + 1")     
  string(SUBSTRING ${CMAKE_CURRENT_SOURCE_DIR} ${lastIndexOfSeperator} -1 currentDirBasename)

  set(TARGET "${currentDirBasename}-${BASEFILE_NAME}")

  add_executable(
    ${TARGET}
    ${BASEFILE_NAME}.c
  )
  target_link_libraries(${TARGET}
    ${LIB_COLLECTIONS} pthread
  )

  install(
    TARGETS ${TARGET}
    DESTINATION ${HOME_BIN_DIR}/tests/${currentDirBasename}
  )

  add_test(
    NAME ${TARGET} 
    COMMAND ${TARGET}
  )

endfunction()

add_subdirectory(linked_list)
add_subdirectory(stack)