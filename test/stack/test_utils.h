#include <stdio.h>

#include "stack.h"

#define LINE_BR printf("\n");

typedef struct {
  int persNr;
  char name[20];
} TPerson;

// Function declarations
#if 1
void
printPerson(const TPerson* pers);
#endif

void
printPerson(const TPerson* pers) {
  if (NULL == pers)
    return;
  printf("PersNr: %d\tname: %s\taddress: %p\n", pers->persNr, pers->name, (void*) pers);
}